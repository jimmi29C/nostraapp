//
//  ItemPresenter.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import RxSwift
import RealmSwift

final class ItemPresenter {
    weak var view: ItemViewControllerDelegate?
    lazy var router: ItemRouterDelegate? = ItemRouter(view: view)
    lazy var interactor: ItemInteractorInputDelegate = ItemInteractor(presenter: self)
    
    let realm = try! Realm()
    
    init(view: ItemViewControllerDelegate?) {
        self.view = view
    }
}

extension ItemPresenter: ItemPresenterDelegate {    
    
    func viewDidLoad() {
        
    }
                
    // Create Features
    func saveItem(category: Category?, name: String, image: Data?) {
        if let currentCategory = category {
            do {
                try self.realm.write {
                    
                    let newItem = Item()
                    newItem.name = name
                    newItem.image = image
                    
                    currentCategory.items.append(newItem)
                }
            } catch {
                print("Error saving new items, \(error)")
            }
        }
        view?.loadItems()
    }
    
    // Update Features
    func changePin(items: Results<Item>?, at indexPath: IndexPath) {

        if let item = items?[indexPath.row] {
            do {
                try self.realm.write{
                    item.isPin = !item.isPin
                }
            } catch {
                print("Error edit pin status, \(error)")
            }
        }
        
        view?.loadItems()
    }
    
    // Delete Features
    func removeItem(items: Results<Item>?, at indexPath: IndexPath) {
        
        if let itemForDeletion = items?[indexPath.row] {
            do {
                try realm.write {
                    realm.delete(itemForDeletion)
                }
            } catch {
                print("Error deleting item, \(error)")
            }
        }
        
        view?.loadItems()
    }
    
}

extension ItemPresenter: ItemInteractorOutputDelegate {
}

