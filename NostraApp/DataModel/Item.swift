//
//  Item.swift
//  NostraApp
//
//  Created by Jimmi on 04/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit
import RealmSwift

class Item: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var image: Data? = nil
    @objc dynamic var isPin: Bool = false
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
