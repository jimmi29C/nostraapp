//
//  FormView.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit
import SnapKit

class FormView: UIView {
    
    let mainView = UIView()
    
    let formView = UIView()
    let nameLbl = UILabel()
    let nameStack = UIStackView()
    let nameTF = UITextField()
    let nameCancelBtn = UIButton()
    
    let addImageView = UIView()
    let addImageIcon = UIImageView()
    let imageBtn = UIButton()
    
    let itemImg = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(mainView)
        mainView.addSubview(formView)
        formView.setupSubviews([nameLbl, nameStack, addImageView, itemImg])
        nameStack.setupArrangedSubviews([nameTF, nameCancelBtn])
        addImageView.setupSubviews([addImageIcon, imageBtn])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        formView.backgroundColor = .white
        formView.layer.cornerRadius = 10
        
        nameLbl.text = "Item"
        
        nameStack.axis = .horizontal
        nameStack.spacing = 12
        
        nameTF.placeholder = "Enter item name ..."
        nameTF.layer.borderWidth = 2
        nameTF.layer.cornerRadius = 10
        nameTF.layer.borderColor = UIColor.black.cgColor
        nameTF.setPadding(leftPadding: 12, rightPadding: 12)
        
        nameCancelBtn.setImage(UIImage(named: "ic_cancel"), for: .normal)
        nameCancelBtn.isHidden = true
        
        addImageView.layer.borderWidth = 2
        addImageView.layer.cornerRadius = 10
        addImageView.layer.borderColor = UIColor.black.cgColor
        
        addImageIcon.image = UIImage(named: "ic_add_image")
        
        itemImg.isHidden = true
    }
    
    private func setupConstraints() {
        
        mainView.snp.makeConstraints { (make) in
            make.top.leading.trailing.bottom.equalTo(self)
        }
        
        formView.snp.makeConstraints { (make) in
            make.centerY.equalTo(mainView)
            make.leading.equalTo(mainView).offset(12)
            make.trailing.equalTo(mainView).offset(-12)
        }
        
        nameLbl.snp.makeConstraints { (make) in
            make.centerY.equalTo(nameTF)
            make.leading.equalTo(formView).offset(12)
        }
        
        nameStack.snp.makeConstraints { (make) in
            make.top.equalTo(formView).offset(12)
            make.leading.equalTo(addImageView)
            make.trailing.equalTo(formView).offset(-12)
        }
        
        nameTF.snp.makeConstraints { (make) in
//            make.top.equalTo(formView).offset(12)
////            make.leading.equalTo(nameLbl.snp.trailing).offset(12)
//            make.leading.equalTo(addImageView)
            make.height.equalTo(40)
        }
        
        nameCancelBtn.snp.makeConstraints { (make) in
//            make.centerY.equalTo(nameTF)
//            make.leading.equalTo(nameTF.snp.trailing).offset(12)
//            make.trailing.equalTo(formView).offset(-12)
            make.size.equalTo(32)
        }
        
        addImageView.snp.makeConstraints { (make) in
            make.top.equalTo(nameTF.snp.bottom).offset(12)
            make.centerX.equalTo(formView)
            make.bottom.equalTo(formView).offset(-12)
            make.size.equalTo(UIScreen.main.bounds.width / 2)
        }
        
        addImageIcon.snp.makeConstraints { (make) in
            make.center.equalTo(addImageView)
            make.size.equalTo(60)
        }
        
        imageBtn.snp.makeConstraints { (make) in
            make.top.leading.trailing.bottom.equalTo(addImageView)
        }
        
        itemImg.snp.makeConstraints { (make) in
            make.edges.equalTo(addImageView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}

