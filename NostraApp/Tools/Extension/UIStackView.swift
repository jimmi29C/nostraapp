//
//  UIStackView.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit

extension UIStackView {
    public func setupArrangedSubviews(_ views: [UIView]) {
        for item in views {
            addArrangedSubview(item)
        }
    }
}
