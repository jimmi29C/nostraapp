//
//  ItemInteractor.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import RxSwift

final class ItemInteractor {
    weak var presenter: ItemInteractorOutputDelegate?
    
    init(presenter: ItemInteractorOutputDelegate) {
        self.presenter = presenter
    }
    
}

extension ItemInteractor: ItemInteractorInputDelegate {
    
}



