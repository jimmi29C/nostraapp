//
//  FormProtocol.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

protocol FormViewControllerDelegate: AnyObject {
    func setMLResult(result: String)
}

protocol FormPresenterDelegate: AnyObject {
    func viewDidLoad()
    
    func detect(image: CIImage)
}

protocol FormRouterDelegate: AnyObject {
    
}

protocol FormInteractorInputDelegate: AnyObject {
    
}

protocol FormInteractorOutputDelegate: AnyObject {
    
}


