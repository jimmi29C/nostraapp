//
//  ItemProtocol.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

protocol ItemViewControllerDelegate: AnyObject {
    func loadItems()
}

protocol ItemPresenterDelegate: AnyObject {
    func viewDidLoad()
    
    func saveItem(category: Category?, name: String, image: Data?)
    
    func changePin(items: Results<Item>?, at indexPath: IndexPath)
    
    func removeItem(items: Results<Item>?, at indexPath: IndexPath)
}

protocol ItemRouterDelegate: AnyObject {
    
}

protocol ItemInteractorInputDelegate: AnyObject {
    
}

protocol ItemInteractorOutputDelegate: AnyObject {
    
}


