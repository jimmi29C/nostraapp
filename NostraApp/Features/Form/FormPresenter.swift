//
//  FormPresenter.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import RxSwift
import RealmSwift
import CoreML
import Vision

final class FormPresenter {
    weak var view: FormViewControllerDelegate?
    lazy var router: FormRouterDelegate? = FormRouter(view: view)
    lazy var interactor: FormInteractorInputDelegate = FormInteractor(presenter: self)
    
    let realm = try! Realm()
    
    init(view: FormViewControllerDelegate?) {
        self.view = view
    }
}

extension FormPresenter: FormPresenterDelegate {
    
    func viewDidLoad() {
        
    }
    
    func detect(image: CIImage) {
        
        // check if there's inception 3 model
        guard let model = try? VNCoreMLModel(for: Inceptionv3().model) else {
            fatalError("loading CoreML Model Failed.")
        }
        
        // make a request variable
        let request = VNCoreMLRequest(model: model) { (request, error) in
            
            // after get the result, check if the coreml can classify the image
            guard let classification = request.results?.first as? VNClassificationObservation else {
                fatalError("Could not classify image")
            }
            
            // separate the result
            let result = classification.identifier.capitalized.components(separatedBy: ",")

            // get the first result
            self.view?.setMLResult(result: result[0])
            
        }
        
        let handler = VNImageRequestHandler(ciImage: image)
        
        do {
            try handler.perform([request])
        } catch {
            print(error)
        }
    }
}

extension FormPresenter: FormInteractorOutputDelegate {
}

