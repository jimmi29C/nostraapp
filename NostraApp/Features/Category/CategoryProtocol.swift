//
//  CategoryProtocol.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

protocol CategoryViewControllerDelegate: AnyObject {
    func loadCategory()
}

protocol CategoryPresenterDelegate: AnyObject {
    func viewDidLoad()
    
    func addCategory()
    
    func saveCategory(data: Category)
    
    func changePin(categories: Results<Category>?, at indexPath: IndexPath)
    
    func removeCategory(categories: Results<Category>?, at indexPath: IndexPath)
    
    func pushToItem(from category: Category?)
}

protocol CategoryRouterDelegate: AnyObject {
    func pushToItem(from category: Category?)
    
    func dismiss(vc: UIAlertController)
    
    func present(vc: UIAlertController)
}

protocol CategoryInteractorInputDelegate: AnyObject {
    
}

protocol CategoryInteractorOutputDelegate: AnyObject {
    
}

