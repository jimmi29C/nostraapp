//
//  ItemCell.swift
//  NostraApp
//
//  Created by Jimmi on 02/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit
import SnapKit

class ItemCell: UITableViewCell {
    
    let mainStack = UIStackView()
    let itemNameLbl = UILabel()
    let pinIcon = UIImageView()
    let itemImg = UIImageView()
    let line = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews([mainStack, line])
        mainStack.setupArrangedSubviews([pinIcon, itemNameLbl, itemImg])
        
        setupCell()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCell() {
        
        selectionStyle = .none
        
        mainStack.axis = .horizontal
        mainStack.spacing = 12
        
        pinIcon.image = UIImage(named: "ic_pin")
        pinIcon.isHidden = true
        
        itemImg.isHidden = true
        
        line.backgroundColor = .darkGray
    }
    
    private func setupConstraints() {
        
        mainStack.snp.makeConstraints { (make) in
            make.top.leading.equalTo(self).offset(12)
            make.trailing.bottom.equalTo(self).offset(-12)
            make.height.equalTo(40)
        }
        
        pinIcon.snp.makeConstraints { (make) in
            make.size.equalTo(32)
        }
        
        itemImg.snp.makeConstraints { (make) in
            make.size.equalTo(40)
        }
        
        line.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(mainStack)
            make.bottom.equalTo(self)
            make.height.equalTo(1)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}
