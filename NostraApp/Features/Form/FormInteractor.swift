//
//  FormInteractor.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import RxSwift

final class FormInteractor {
    weak var presenter: FormInteractorOutputDelegate?
    
    init(presenter: FormInteractorOutputDelegate) {
        self.presenter = presenter
    }
    
}

extension FormInteractor: FormInteractorInputDelegate {
    
}
