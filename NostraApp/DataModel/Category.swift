//
//  Category.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit
import RealmSwift

class Category: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var isPin: Bool = false
    let items = List<Item>()
}
