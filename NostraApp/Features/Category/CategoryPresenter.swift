//
//  CategoryPresenter.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import RxSwift
import RealmSwift

final class CategoryPresenter {
    weak var view: CategoryViewControllerDelegate?
    lazy var router: CategoryRouterDelegate? = CategoryRouter(view: view)
    lazy var interactor: CategoryInteractorInputDelegate = CategoryInteractor(presenter: self)
    
    let realm = try! Realm()
    
    init(view: CategoryViewControllerDelegate?) {
        self.view = view
    }
}

extension CategoryPresenter: CategoryPresenterDelegate {
    
    func pushToItem(from category: Category?) {
        router?.pushToItem(from: category)
    }
    
    func viewDidLoad() {
        
    }
    
    func addCategory() {
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add New Category", message: "", preferredStyle: UIAlertController.Style.alert)
        
        let addAction = UIAlertAction(title: "Add Category", style: UIAlertAction.Style.default) { (action) in
            let newCategory = Category()
            newCategory.name = textField.text ?? ""
            
            self.saveCategory(data: newCategory)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
            self.router?.dismiss(vc: alert)
        }
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create new category"
            
            textField = alertTextField
        }

        alert.addAction(addAction)
        alert.addAction(cancelAction)
        
        router?.present(vc: alert)
        
    }
    
    // Create Features
    func saveCategory(data: Category) {
        
        do {
            try realm.write {
                realm.add(data)
            }
        } catch {
            print("Error while saving category \(error)")
        }
        
        view?.loadCategory()
    }
    
    
    // Update Features
    func changePin(categories: Results<Category>?, at indexPath: IndexPath) {

        if let category = categories?[indexPath.row] {
            do {
                try self.realm.write{
                    category.isPin = !category.isPin
                }
            } catch {
                print("Error edit pin status, \(error)")
            }
        }
        
        view?.loadCategory()
    }
    
    // Delete Features
    func removeCategory(categories: Results<Category>?, at indexPath: IndexPath) {
        
        if let categoryForDeletion = categories?[indexPath.row] {
            do {
                try realm.write {
                    realm.delete(categoryForDeletion)
                }
            } catch {
                print("Error deleting category, \(error)")
            }
        }
        
        view?.loadCategory()
    }
    
    
}

extension CategoryPresenter: CategoryInteractorOutputDelegate {
}
