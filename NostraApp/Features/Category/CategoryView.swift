//
//  HomeView.swift
//  Nostra
//
//  Created by Jimmi on 02/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit
import SnapKit

class CategoryView: UIView {
    
    let mainView = UIView()
    
    let searchBar = UISearchBar()
    let categoryTable = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews([mainView])
        mainView.setupSubviews([searchBar, categoryTable])
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        mainView.backgroundColor = UIColor.green
        
        
        categoryTable.separatorStyle = .none
        categoryTable.register(ItemCell.self, forCellReuseIdentifier: "cell")
    }
    
    private func setupConstraints() {
        
        mainView.snp.makeConstraints { (make) in
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin)
            make.leading.trailing.equalTo(self)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin)
        }
        
        searchBar.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(mainView)
        }
        
        categoryTable.snp.makeConstraints { (make) in
            make.top.equalTo(searchBar.snp.bottom)
            make.leading.trailing.bottom.equalTo(mainView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
}
