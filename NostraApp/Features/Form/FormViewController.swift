//
//  FormViewController.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CoreML
import Vision

class FormViewController: UIViewController, UINavigationControllerDelegate {
    
    let root = FormView()
    
    lazy var presenter: FormPresenterDelegate = FormPresenter(view: self)
    
    var disposeBag = DisposeBag()
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerController.SourceType.camera
        
        setRX()
    }
    
    // MARK: - Using RxSwift in textfield and button for more reactive usage
    private func setRX() {
        
        root.nameTF.rx.text.subscribe(onNext: { (text) in
            if text!.isEmpty {
                self.root.nameCancelBtn.hideAnimated(in: self.root.nameStack)
            } else {
                self.root.nameCancelBtn.showAnimated(in: self.root.nameStack)
            }
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        root.imageBtn.rx.tap.subscribe(onNext: { (_) in
            self.present(self.imagePicker, animated: true, completion: nil)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        root.nameCancelBtn.rx.tap.subscribe(onNext: { (_) in
            self.root.nameTF.text = nil
            self.root.nameCancelBtn.hideAnimated(in: self.root.nameStack)
            self.root.nameTF.resignFirstResponder()
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
    }
    
    public func reload() {
        
        root.nameTF.text = nil
        
        root.itemImg.image = nil
        root.itemImg.isHidden = true
    }
}

extension FormViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let userPickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
            root.itemImg.image = userPickedImage
            root.itemImg.isHidden = false
            
            guard let convertedCIImage = CIImage(image: userPickedImage) else {
                fatalError("cannot convert to CIImage")
            }
            
            // after get the image from camera, passing to detect function
            presenter.detect(image: convertedCIImage)
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
}

extension FormViewController: FormViewControllerDelegate {
    func setMLResult(result: String) {
        DispatchQueue.main.async {
            self.root.nameTF.text = result
        }
    }
}
