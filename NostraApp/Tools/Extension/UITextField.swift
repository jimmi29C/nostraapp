//
//  UITextField.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit

extension UITextField {
    
    func setLeftPadding() {
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 12, height: 2.0))
        self.leftView = leftView
        self.leftViewMode = .always
    }
    
    func setPadding(leftPadding: CGFloat = 0.0, rightPadding: CGFloat = 0.0) {
        if leftPadding != 0.0 {
            self.leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: leftPadding, height: 2.0))
            self.leftViewMode = .always
        }
        if rightPadding != 0.0 {
            self.rightView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: rightPadding, height: 2.0))
            self.rightViewMode = .always
        }
//        self.leftViewRect(forBounds: CGRect(x: 0.0, y: 0.0, width: leftPadding, height: 2.0))
//        self.rightViewRect(forBounds: CGRect(x: 0.0, y: 0.0, width: rightPadding, height: 2.0))
    }
}
