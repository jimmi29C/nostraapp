//
//  CategoryViewController.swift
//  Nostra
//
//  Created by Jimmi on 02/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxCocoa

class CategoryViewController: UIViewController {
    
    let root = CategoryView()
    
    lazy var presenter: CategoryPresenterDelegate = CategoryPresenter(view: self)
    
    let realm = try! Realm()
    
    var categories: Results<Category>?
    
    var disposeBag = DisposeBag()
    
    var addBtn: UIBarButtonItem? {
        didSet {
            addBtn?.rx.tap.subscribe(onNext: { (_) in
                self.presenter.addCategory()
            }, onError: { (error) in
                print(error)
            }).disposed(by: disposeBag)
            navigationItem.rightBarButtonItem = addBtn
        }
    }
    
    var isForm: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        self.view = root
        self.title = "Category"
        
        setAddButton()
        
        root.categoryTable.delegate = self
        root.categoryTable.dataSource = self
        
        root.searchBar.delegate = self
        
        loadCategory()
    }
    
    private func setAddButton() {
        self.addBtn = UIBarButtonItem(barButtonSystemItem: self.isForm ? .done : .add, target: self, action: nil)
    }
}

extension CategoryViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: - Tableview DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.count == 0 ? 1 : categories?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ItemCell
        
        if categories?.count != 0, let category = categories?[indexPath.row] {
            cell.itemNameLbl.text = category.name
            
            cell.pinIcon.isHidden = !category.isPin
        } else {
            cell.itemNameLbl.text = "No category"
        }
        
        return cell
    }
    
    // MARK: - Tableview Delegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if categories!.count > 0 {
            presenter.pushToItem(from: categories?[indexPath.row])
        }
    }
    
    // MARK: - Scroll the cell from right to delete
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { action, index in
            self.presenter.removeCategory(categories: self.categories, at: indexPath)
        }
        delete.backgroundColor = .red

        return [delete]
    }
    
    // MARK: - Scroll the cell from left to pin
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let pinAction = UIContextualAction(style: .normal, title:  "Pin", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.presenter.changePin(categories: self.categories, at: indexPath)
            success(true)
        })
        pinAction.backgroundColor = .blue
        
        return UISwipeActionsConfiguration(actions: [pinAction])
    }
    
    // MARK:
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension CategoryViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.isEmpty {
            loadCategory()
        } else {
            categories = realm.objects(Category.self).filter("name CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "isPin", ascending: false)
            root.categoryTable.reloadData()
        }
    }
}

extension CategoryViewController: CategoryViewControllerDelegate {    
    
    // Read Features
    func loadCategory() {
        categories = realm.objects(Category.self).sorted(byKeyPath: "isPin", ascending: false)
        root.categoryTable.reloadData()
    }
}
