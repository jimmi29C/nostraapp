//
//  UIView.swift
//  Nostra
//
//  Created by Jimmi on 02/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit

extension UIView {
    public func setupSubviews(_ views: [UIView]) {
        for item in views {
            addSubview(item)
        }
    }
    
    func hideAnimated(in stackView: UIStackView) {
        if !self.isHidden {
            self.isHidden = true
            UIView.animate(
                withDuration: 0.35,
                delay: 0,
                usingSpringWithDamping: 0.9,
                initialSpringVelocity: 1,
                options: [],
                animations: {
                    stackView.layoutIfNeeded()
                },
                completion: nil
            )
        }
    }

    func showAnimated(in stackView: UIStackView) {
        if self.isHidden {
            UIView.animate(
                withDuration: 0.35,
                delay: 0,
                usingSpringWithDamping: 0.9,
                initialSpringVelocity: 1,
                options: [],
                animations: {
                    self.isHidden = false
                    stackView.layoutIfNeeded()
                },
                completion: nil
            )
        }
    }

}
