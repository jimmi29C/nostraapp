//
//  CategoryInteractor.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import RxSwift

final class CategoryInteractor {
    weak var presenter: CategoryInteractorOutputDelegate?
    
    init(presenter: CategoryInteractorOutputDelegate) {
        self.presenter = presenter
    }
    
}

extension CategoryInteractor: CategoryInteractorInputDelegate {
    
}


