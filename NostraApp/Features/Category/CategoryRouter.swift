//
//  CategoryRouter.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit

final class CategoryRouter: CategoryRouterDelegate {
    
    weak var source: UIViewController?
    
    init(view: CategoryViewControllerDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToItem(from category: Category?) {
        let vc = ItemViewController()
        vc.selectedCategory = category
        source?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func dismiss(vc: UIAlertController) {
        vc.dismiss(animated: true, completion: nil)
    }
    
    func present(vc: UIAlertController) {
        source?.present(vc, animated: true, completion: nil)
    }
}


