//
//  ItemViewController.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxCocoa

class ItemViewController: UIViewController {
    
    let root = ItemView()
    
    lazy var presenter: ItemPresenterDelegate = ItemPresenter(view: self)
    
    var items: Results<Item>?
    let realm = try! Realm()
    var selectedCategory: Category? {
        didSet {
            loadItems()
        }
    }
    
    var formVC = FormViewController()
        
    var addBtn: UIBarButtonItem? {
        didSet {
            addBtn?.rx.tap.subscribe(onNext: { (_) in
                self.addBtnTapped()
            }, onError: { (error) in
                print(error)
            }).disposed(by: disposeBag)
            navigationItem.rightBarButtonItem = addBtn
        }
    }
    
    var isForm: Bool = false
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        self.title = "Item"
        
        navigationItem.rightBarButtonItem = root.cameraBarButton
        
        root.searchBar.delegate = self
        
        root.itemTable.dataSource = self
        root.itemTable.delegate = self
        
        setAddButton()
    }
    
    private func setAddButton() {
        self.addBtn = UIBarButtonItem(barButtonSystemItem: self.isForm ? .done : .add, target: self, action: nil)
    }
    
    private func addBtnTapped() {
        if isForm {
            
            if let text = formVC.root.nameTF.text, !text.isEmpty {
                presenter.saveItem(category: self.selectedCategory, name: text, image: formVC.root.itemImg.image?.pngData())
            }
            
            formVC.reload()
            formVC.willMove(toParent: nil)
            formVC.removeFromParent()
            UIView.transition(with: root, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                self.formVC.view.removeFromSuperview()
            }, completion: nil)
            root.searchBar.text = nil
            
        } else {
            addChild(formVC)
            formVC.view.frame = root.frame
//            formVC.root.nameTF.becomeFirstResponder()
            UIView.transition(with: root, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                self.root.addSubview(self.formVC.view)
            }, completion: nil)
            formVC.didMove(toParent: self)
        }
        isForm = !isForm
        setAddButton()
    }
}

extension ItemViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Tableview DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count == 0 ? 1 : items?.count ?? 1
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "item-cell", for: indexPath) as! ItemCell
        
        if items?.count != 0, let item = items?[indexPath.row] {
            cell.itemNameLbl.text = item.name
            cell.pinIcon.isHidden = !item.isPin
            if let data = item.image,
                let image = UIImage(data: data) {
                cell.itemImg.image = image
                cell.itemImg.isHidden = false
            } else {
                cell.itemImg.isHidden = true
            }
        } else {
            cell.itemNameLbl.text = "No Item"
        }
        
        return cell
    }
    
    // MARK: - Tableview Delegate Methods    
    
    // MARK: - Scroll the cell from right to delete
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { action, index in
            self.presenter.removeItem(items: self.items, at: indexPath)
        }
        delete.backgroundColor = .red

        return [delete]
    }
    
    // MARK: - Scroll the cell from left to pin
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let pinAction = UIContextualAction(style: .normal, title:  "Pin", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.presenter.changePin(items: self.items, at: indexPath)
            success(true)
        })
        pinAction.backgroundColor = .blue
        return UISwipeActionsConfiguration(actions: [pinAction])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}

extension ItemViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.isEmpty {
            loadItems()
        } else {
            items = selectedCategory?.items.filter("name CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "isPin", ascending: false)
            root.itemTable.reloadData()
        }
    }
}

extension ItemViewController: ItemViewControllerDelegate {
    
    // Read Features
    func loadItems() {
        items = selectedCategory?.items.sorted(byKeyPath: "isPin", ascending: false)
        root.itemTable.reloadData()
    }
}

