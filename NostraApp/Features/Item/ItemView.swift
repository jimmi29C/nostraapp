//
//  ItemView.swift
//  NostraApp
//
//  Created by Jimmi on 05/07/20.
//  Copyright © 2020 Jimmi. All rights reserved.
//

import UIKit

class ItemView: UIView {
    
    let cameraBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: nil)
    
    let mainView = UIView()
    let searchBar = UISearchBar()
    
    let itemTable = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(mainView)
        mainView.setupSubviews([searchBar, itemTable])
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        
        itemTable.separatorStyle = .none
        itemTable.register(ItemCell.self, forCellReuseIdentifier: "item-cell")
    }
    
    private func setupConstraints() {
        
        mainView.snp.makeConstraints { (make) in
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin)
            make.leading.trailing.equalTo(self)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin)
        }
        
        searchBar.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(mainView)
        }
        
        itemTable.snp.makeConstraints { (make) in
            make.top.equalTo(searchBar.snp.bottom)
            make.leading.trailing.bottom.equalTo(mainView)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}

